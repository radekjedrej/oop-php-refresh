<?php


class Users {

  public $first;
  public $last;
  public $hairColor;
  public $eyeColor;

  public function __construct($first, $last, $hairColor, $eyeColor) {
    $this->first = $first;
    $this->last = $last;
    $this->hairColor = $hairColor;
    $this->EyeColor = $eyeColor;
  }

  public function __destruct() {
    
  }
}